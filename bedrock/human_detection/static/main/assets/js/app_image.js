
let drawCanvasImage = document.getElementById("idraw-canvas-image");
let selectedImage = document.getElementById('selected-image');
let baseImage ;
$(document).ready(function () {
  $("#imgInp").change(function () {
    readURL(this);
    $('#draw-canvas-image').hide();
    
    setTimeout(() => {
      drawImage()
      captureImage()
    }, 100);
  });
});
function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function (e) {
      // baseImage = e.target.result;
      // baseImage = baseImage.slice(22);
      $("#selected-image").show();
      $("#selected-image").attr("src", e.target.result);
      // console.log(baseImage,'read')
    };
    reader.readAsDataURL(input.files[0]);
  }
}


function drawImage(){
  drawCanvasImage = document.getElementById("draw-canvas-image");
  drawCanvasImage.width = $('#selected-image').width();
  drawCanvasImage.height = $('#selected-image').height();
  var ctx = drawCanvasImage.getContext("2d");
  selectedImage = document.getElementById("selected-image");

  ctx.drawImage(selectedImage, 0, 0, $('#selected-image').width(),$('#selected-image').height());

  baseImage = drawCanvasImage.toDataURL().slice(22)
  // console.log(baseImage,'baseImage')
}




function captureImage() {
  // console.log(baseImage,'sent')
  $.ajax({
    url: "/api/product-detect/",
    method: "post",
    data: {
      image: baseImage,
      csrfmiddlewaretoken: "{{ csrf_token }}",
    },
    beforeSend: function() {
      $('.loader-section').show()
    },
    success: function (response) {
      // console.log(response,'response')
      // $('#draw-canvas-image').show();
      $('.product-list-section').show();
      let productList = response.data;
      $('.product-list ul').empty()
      // print product name 
      let productListName = response.counter;
      let colorCodes = response.color_codes;
      // $('.product-list-heading span').text("("+response.total_products+")")
      $('.product-list-heading span').text("("+response.total_detects+")")
      $.each(productListName, function (key, value) {
        console.log(colorCodes[key])
        // $('.product-list ul').append(`<li style="color:${colorCodes[key]};"><b>`+key+`:</b> `+value + `</li>`)
      })

      // show image
      image_base64_encoded = response.image_base64_encode;
      image = new Image();
      image.src = 'data:image/jpg;base64,' + image_base64_encoded;
      image.onload = function () {
        $('.video-box').css('height', 'max-content');
        $('#selected-image').css('margin-top', '15px');
        $('#selected-image').css('margin-bottom', '15px');
        $('#selected-image').attr('src', image.src)
      };
     
      $('.loader-section').hide()
      
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) { 
      // captureImage();
  } 
  });
}
