let captureStatus = 0
$(document).ready(function () {
  $("#imgInp").change(function () {
    captureStatus=0
    readURL(this);
    $('#draw-canvas').hide();
  });
  $('#selected-video').bind('play', function (e) {
    // do something
    captureStatus=1
    captureFromVideo()
  })
  $('#selected-video').on('ended',function(){
    // do something
    captureStatus = 0
    $('#draw-canvas').hide();
  })
  $('#selected-video').on('pause',function(){
    // do something
    captureStatus = 0
    $('#draw-canvas').hide();
  })
});
let canvas = document.getElementById("video-canvas");
let video = document.getElementById("selected-video");
let videoHeight, videoWidth;
function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function (e) {
      $("#selected-video").show();
      $("#selected-video").attr("src", e.target.result);

    };
    reader.readAsDataURL(input.files[0]);
  }
}

function captureFromVideo() {
  $('#draw-canvas').show();
  videoHeight = $(".video-box").height();
  videoWidth = $(".video-box").width();
  let image = "";
  canvas.width = videoWidth;
  canvas.height = videoHeight;
  let ctx = canvas.getContext("2d");
  ctx.drawImage(video, 0, 0, canvas.width, canvas.height);
  image = canvas.toDataURL("image/jpeg");
  let baseImage = image.slice(22);
  $.ajax({
    url: "/api/detect/",
    method: "post",
    data: {
      image: baseImage,
      csrfmiddlewaretoken: "{{ csrf_token }}",
    },
    success: function (response) {
      console.log(response.data);
      let fish = response.data;
      let drawCanvas = document.getElementById("draw-canvas");
      let drawCanvasCtx = drawCanvas.getContext("2d");
      drawCanvas.width = videoWidth;
      drawCanvas.height = videoHeight;
      drawCanvasCtx.clearRect(0, 0, videoWidth, videoHeight);
      $.each(fish, function (key, value) {
        console.log(value, "value", key, "key");
        let fishClass = value.class;
        let fishConfidence = value.confidence;
        drawCanvasCtx.font = "20px serif";
        drawCanvasCtx.fillStyle = "red";
        drawCanvasCtx.fillText(
          `${fishClass} ${fishConfidence}`,
          value.box[0]+5,
          value.box[1] +20
        );
        drawCanvasCtx.beginPath();
        drawCanvasCtx.strokeStyle = "#74E6A7";
        drawCanvasCtx.lineWidth = 4;
        drawCanvasCtx.rect(
          value.box[0],
          value.box[1],
          value.box[2] - value.box[0],
          value.box[3] - value.box[1]
        );
        drawCanvasCtx.stroke();
      });
      if (captureStatus==1){
        captureFromVideo();
      }
    },
  });
}
