import os
import uuid
import cv2
import base64
from collections import Counter
from ultralytics import YOLO

from object_names import object_list, colors

from django.conf import settings
ROOT = settings.MEDIA_ROOT 

# Detection model path
model = YOLO('yolov8m.pt')
conf = 0.40 #confidence level 
img_resolution = 640 

def bgr2hex(color):
    b, g, r = color
    return "#{:02x}{:02x}{:02x}".format(r,g,b)

def counter(classes): 
	counts = dict(Counter(classes))
	duplicates = {key:value for key, value in counts.items() if value > 0}
	return duplicates

def detect(img):
	results = model.predict(source=img, 
							imgsz=img_resolution, 
							conf=conf,
							# save=True,
							# show_labels=True,
							classes = 0,
							
									)   
	tensor_list = results[0].boxes.data
	detection = tensor_list.tolist()
	total_object = len(detection)
	# filename = f"{uuid.uuid4().hex}.png"
	# savepath = os.path.join(ROOT, filename)
	cal = []
	color_codes = set()
	for det in detection:
		x,y,w,h,confidence,cls = [int(d) for d in det]
		cal.append(str(object_list[int(cls)]))
		img = cv2.rectangle(img,(x,y),(w,h), colors[int(cls)], 2)
		hex_code = bgr2hex(colors[int(cls)])
		color_codes.add((str(object_list[int(cls)]), hex_code))
		cv2.putText(img, str(object_list[int(cls)]), (x, y-10), 4, 1, (0, 165, 255), 2)
	dash, buffer = cv2.imencode('.jpg', img)
	image_base64_encode = base64.b64encode(buffer)
	image_base64_encode = image_base64_encode.decode('utf-8')
	object_count = dict(sorted((counter(cal)).items()))
	color_codes = {elem[0]:elem[1] for elem in color_codes}
	return image_base64_encode, total_object, color_codes
	# return True
