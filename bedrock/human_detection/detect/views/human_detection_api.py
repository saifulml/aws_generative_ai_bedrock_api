from rest_framework.response import Response
from human_detect import detect
 
from rest_framework.views import APIView

### libraries
import base64
import numpy as np
import cv2
from collections import Counter
# import traceback
import datetime

# DetectObjectView
class DetectHuman(APIView):
    def post(self, request, format=None):
        filename = None
 
        total_detects = 0
        note = ''
        status = 200
        try:
            image = request.data.get('image')
            description = request.TextField(null=True, blank=True)
            image_base64_decode = base64.b64decode(image)
            im_arr = np.frombuffer(image_base64_decode, dtype=np.uint8)
            image_np = cv2.imdecode(im_arr, flags=cv2.IMREAD_COLOR) 
            # noise = cv2.imread(image_np)

            # Detect object
            image_base64_encode, total_detects, color_codes = detect(image_np)



        except Exception as e:
            # note = str(e)
            note = f"(Occurred at: {datetime.datetime.now()}) {str(e)} "
            # with open ('reason.txt', 'a') as w:
            #     w.write(str(note + '\n'))
            with open('exception_log.txt', 'a') as log_file:
                log_file.write(note + '\n')
            # traceback.print_exc()
            print('okkkkkkkkkkkkkkkkk')
            status = 400
            print('here *************************************',note)
        return Response({'status':status,'image_base64_encode': image_base64_encode, 'note': note,  "total_detects": total_detects, "color_codes": color_codes}, status=status)
