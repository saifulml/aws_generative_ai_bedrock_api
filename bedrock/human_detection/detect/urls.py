from django.urls import path
from .views import DetectHuman, human_detect


urlpatterns = [
    path('api/object-detect/', DetectHuman.as_view(), name='detect-object'),
    path('', human_detect, name='detect'),
]