import random 
 
object_dict = {
    0: 'Human'
    }

##objects list
object_list = list(object_dict.values())
##Generate colors for every objects
colors = [tuple(random.choices([i for i in range(256)], k=3)) for _ in object_list]
